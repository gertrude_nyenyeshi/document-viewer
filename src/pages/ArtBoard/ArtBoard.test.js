import { render, screen } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router, Route } from 'react-router-dom';

import ArtBoard from './ArtBoard';

describe('Renders ArtBoard', () => {
  test('Renders correctly', () => {
    const dataProps = {
      artboards: {
        entries: [
          {
            name: 'Screen One',
            files: [
              { height: 900, width: 900, url: 'test.com' },
              { height: 900, width: 900, url: 'test.com' },
            ],
          },
          {
            name: 'Screen Two',
            files: [
              { height: 900, width: 900, url: 'test.com' },
              { height: 900, width: 900, url: 'test.com' },
            ],
          },
        ],
      },
    };

    const history = createMemoryHistory();
    history.push('/artboard/1');

    render(
      <Router history={history}>
        <Route
          path={'/artboard/:id'}
          render={() => <ArtBoard artboards={dataProps.artboards} />}
        />
      </Router>
    );
    expect(screen.getByText('Screen Two')).toBeInTheDocument();
  });
});

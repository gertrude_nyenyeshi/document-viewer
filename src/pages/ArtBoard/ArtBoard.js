import React, { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Link, useHistory } from 'react-router-dom';
import _ from 'lodash';
import './ArtBoard.scss';

const ArtBoard = ({ artboards }) => {
  let { id } = useParams();
  const history = useHistory();

  const [selectedFile, setSelected] = useState();
  const [screenWidth, setScreenWidth] = useState();

  // Fetch current screen based on id param
  let screen = artboards.entries[id];

  let currentScreen;

  useEffect(() => {
    setScreenWidth(document.querySelector('.screen-size').offsetWidth);

    currentScreen = screen.files.filter((file) => file.width === screenWidth);

    // If width doesn't fit set element width
    if (!currentScreen.width) {
      // Choose file with larger width
      currentScreen = _.maxBy(screen.files, function (file) {
        return file.width;
      });
    }

    setSelected(currentScreen);
  });

  const moveLocation = (num) => {
    if (num === +1) {
      id = Number(id) + 1;

      if (id > artboards.entries.length - 1) {
        // Start the loop if user gets to end of entries
        id = 0;
        history.push(`/artboard/${id}`);
      }
      history.push(`/artboard/${id}`);
    } else {
      id = Number(id) - 1;

      if (id < 0) {
        id = artboards.entries.length - 1;
        history.push(`/artboard/${id}`);
      }
      history.push(`/artboard/${id}`);
    }
  };

  return (
    <Fragment>
      <header className="screen-header">
        <div className="direction-buttons">
          <Link to="/">
            <button>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </Link>

          <button onClick={() => moveLocation(-1)}>
            <i className="fa fa-arrow-left" aria-hidden="true"></i>
          </button>
          <span>{`${id}/${artboards.entries.length - 1}`}</span>
          <button onClick={() => moveLocation(+1)}>
            <i className="fa fa-arrow-right" aria-hidden="true"></i>
          </button>
        </div>
        <div className="screenname-section">
          <p className="screen-name">{screen.name}</p>
        </div>
      </header>
      <section className="screen-section">
        <img
          alt={selectedFile ? selectedFile.name : ''}
          className="screen-size"
          src={selectedFile ? selectedFile.url : ''}
        />
      </section>
    </Fragment>
  );
};

export default ArtBoard;

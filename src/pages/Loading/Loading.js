import React, { Fragment } from 'react';
import './Loading.scss';

const Loading = () => (
  <Fragment>
    <header className="header-loading"></header>
    <div className="loader"></div>
    <section>
      <ul className="section-loading">
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
        <li className="thumbnail-loading"></li>
      </ul>
    </section>
  </Fragment>
);

export default Loading;

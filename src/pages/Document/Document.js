import React, { Fragment } from 'react';
import Thumbnail from '../../components/Thumbnail/Thumbnail';
import './Document.scss';

const Document = ({ document }) => {
  return (
    <Fragment>
      <header className="document-header">
        <h1>Document View</h1>
        <p>Document Name: {document.name}</p>
      </header>
      <section className="document-section">
        <ul>
          {document.artboards.entries.map((artboard, index) => (
            <Thumbnail key={index} pos={index} artboard={artboard} />
          ))}
        </ul>
      </section>
    </Fragment>
  );
};

export default Document;

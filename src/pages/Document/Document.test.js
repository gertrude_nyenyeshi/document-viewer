import { render, screen } from '@testing-library/react';

import Document from './Document';

describe('Renders Document', () => {
  test('Renders correctly', () => {
    const dataProps = {
      name: 'Test',
      artboards: {
        entries: [],
      },
    };

    render(<Document document={dataProps} />);
    expect(
      screen.getByText(`Document Name: ${dataProps.name}`)
    ).toBeInTheDocument();
  });
});

import { render, screen } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import App from './App';

describe('Renders App', () => {
  test('Renders correctly', () => {
    const history = createMemoryHistory();

    const dataProps = {
      data: {
        share: { version: { document: { artboards: { entries: [] } } } },
      },
    };

    render(
      <Router history={history}>
        <App data={dataProps.data} />
      </Router>
    );
    expect(screen.getByText('Document View')).toBeInTheDocument();
  });
});

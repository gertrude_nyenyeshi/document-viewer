import React from 'react';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';
import Loading from '../../pages/Loading/Loading';
import App from './App';

const FECTH_DATA = gql`
  {
    share(shortId: "Y8wDM") {
      shortId
      version {
        document {
          name
          artboards {
            entries {
              name
              isArtboard
              files {
                url
                height
                width
                scale
                thumbnails {
                  url
                  height
                  width
                }
              }
            }
          }
        }
      }
    }
  }
`;

const AppContainer = () => (
  <Query query={FECTH_DATA}>
    {({ loading, error, data }) => {
      if (error) return <p>Ooops..something went wrong</p>;
      if (loading) return <Loading />;
      return <App data={data} />;
    }}
  </Query>
);

export default AppContainer;

import { Route, Switch } from 'react-router-dom';
import ArtBoard from '../../pages/ArtBoard/ArtBoard';
import Document from '../../pages/Document/Document';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';
import './App.scss';

function App({ data }) {
  return (
    <main>
      <Switch>
        <ErrorBoundary>
          <Route
            exact
            path="/"
            render={() => <Document document={data.share.version.document} />}
          />
          <Route
            path="/artboard/:id"
            render={() => (
              <ArtBoard artboards={data.share.version.document.artboards} />
            )}
          />
        </ErrorBoundary>
      </Switch>
    </main>
  );
}

export default App;

import { render, screen } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

import Thumbnail from './Thumbnail';

describe('Renders Thumbnail', () => {
  test('Renders correctly', () => {
    const dataProps = {
      name: 'Test Artboard',
      files: [
        {
          height: 900,
          width: 900,
          url: 'test.com',
          thumbnails: [{ height: 300, width: 300, url: 'test.com' }],
        },
        {
          height: 900,
          width: 900,
          url: 'test.com',
          thumbnails: [{ height: 300, width: 300, url: 'test.com' }],
        },
      ],
    };

    const history = createMemoryHistory();

    render(
      <Router history={history}>
        <Thumbnail pos={1} artboard={dataProps} />
      </Router>
    );
    expect(screen.getByText('Test Artboard')).toBeInTheDocument();
  });
});

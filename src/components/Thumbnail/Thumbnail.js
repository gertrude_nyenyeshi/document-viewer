import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import './Thumbnail.scss';

const Thumbnail = ({ artboard, pos }) => {
  // Get the smaller file size
  let smallFile = _.minBy(artboard.files, function (file) {
    return file.width;
  });

  const thumbnail = smallFile.thumbnails[0].url;
  return (
    <Link to={`/artboard/${pos}`}>
      <li>
        <img alt="" src={thumbnail} />
        <p>{artboard.name}</p>
      </li>
    </Link>
  );
};

export default Thumbnail;

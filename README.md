### Document Viewer

View all your document projects seamlessly

#### Structure:

1. Technologies.
2. How it Works (Screenshots + GIFs)
3. Installation Instructions.
4. Final Words/Thoughts

### Technologies.

This project is built using React and Apollo and tested using React Testing Library.

### How It Works

#### GIF

[![document-viewer.gif](https://i.postimg.cc/R0Dq9L1h/document-viewer.gif)](https://postimg.cc/d7GJjdxc)

#### Screenshots

[![Screenshot-2020-11-03-at-14-08-38.png](https://i.postimg.cc/rsJky86j/Screenshot-2020-11-03-at-14-08-38.png)](https://postimg.cc/ctrjXGLt)

[![Screenshot-2020-11-03-at-14-07-40.png](https://i.postimg.cc/SKMb2LFJ/Screenshot-2020-11-03-at-14-07-40.png)](https://postimg.cc/67tmFvQX)

[![Screenshot-2020-11-03-at-14-07-09.png](https://i.postimg.cc/j56VvsjT/Screenshot-2020-11-03-at-14-07-09.png)](https://postimg.cc/cgJ5LGYk)

[![Screenshot-2020-11-03-at-14-08-08.png](https://i.postimg.cc/3xkPVjwn/Screenshot-2020-11-03-at-14-08-08.png)](https://postimg.cc/tsHSVxWx)

[![Screenshot-2020-11-03-at-14-07-25.png](https://i.postimg.cc/mrQ6WmWN/Screenshot-2020-11-03-at-14-07-25.png)](https://postimg.cc/Lq8VzBXn)

### Installation Instructions

1. Clone the Repository to your local machine.
2. Run `npm i`
3. Run `npm run start` or `yarn start`
4. Run `yarn test` to run tests

### Final Words

#### Decisions

1. I used Lodash to help with filtering through the data i.e Selecting the image to use as a thumbnail and main view.
2. In terms of file structure, all screens in different routes are placed in the Pages folder, and the rest in the Components folder.

#### Improvements

1. I made the app as responsive as possible but would have loved to spend more time on the styling.
